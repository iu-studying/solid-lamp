package switches;


public abstract class SimpleSwitcher<SwitchableDevice extends ISwitchableDevice> implements ISwitcher {
    private SwitchableDevice switchableDevice;


    public SimpleSwitcher(SwitchableDevice switchableDevice) {
        this.switchableDevice = switchableDevice;
    }


    @Override
    public void turnOn() {
        switchableDevice.enable();
    }

    @Override
    public void turnOff() {
        switchableDevice.disable();
    }
}
