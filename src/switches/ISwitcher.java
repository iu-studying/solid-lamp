package switches;


public interface ISwitcher {
    void turnOn();
    void turnOff();
}
