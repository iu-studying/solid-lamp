package switches;


public interface ISwitchableDevice {
    void enable();
    void disable();
}
