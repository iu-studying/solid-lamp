package lamp;

import switches.ISwitchableDevice;


public class Lamp implements ISwitchableDevice {
    @Override
    public void enable() {
        System.out.println("Lamp is turned on");
    }

    @Override
    public void disable() {
        System.out.println("Lamp is turned off");
    }
}
