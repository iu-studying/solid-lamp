package lamp;

import switches.ISwitchableDevice;
import switches.SimpleSwitcher;


public class LampSwitcher extends SimpleSwitcher<ISwitchableDevice> {
    public LampSwitcher(ISwitchableDevice switchableDevice) {
        super(switchableDevice);
    }
}
