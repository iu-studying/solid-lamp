import lamp.Lamp;
import lamp.LampSwitcher;
import switches.ISwitchableDevice;
import switches.ISwitcher;


public class Main {
    public static void main(String[] args) {
        ISwitchableDevice lamp = new Lamp();
        ISwitcher switcher = new LampSwitcher(lamp);
        switcher.turnOn();
        switcher.turnOff();
    }
}
